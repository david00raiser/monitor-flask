from xml.dom import minidom
from global_config import folders_path
from CustomException import CustomException

class XMLParser:
    'Class for xml file treatments and parsing'

    def __init__(self):
        self.XMLfiles = []
        self.folders = []
        self.jobs = []

    def scan_files(self):
        import os
        print("Scanning directory: {}".format(folders_path))

        try:
            for root, dirs, files in os.walk(folders_path):

                for filename in files:
                    self.XMLfiles.append(filename)
                    print("Found file: {}".format(filename))
        except:
            raise CustomException("Problem at accesing OS")

    def analyze_files(self):
        if len(self.XMLfiles) == 0:
            print("No XML files to analyze")
            return False

        for xml_file in self.XMLfiles:
            file = "{}/{}".format(folders_path, xml_file)
            xml = minidom.parse(file.format(xml_file))

            #folder iterating
            foldersDOM = xml.getElementsByTagName('FOLDER')
            if(len(foldersDOM)>0):
                for folder in foldersDOM:
                    folder_doct = dict(folder.attributes.items())
                    self.folders.append(folder_doct)

                    #jobs iterating
                    jobsDOM = folder.getElementsByTagName("JOB")
                    for job in jobsDOM:
                        job_dict = dict(job.attributes.items())

                        job_intern_tags = ['VARIABLE', 'INCOND', 'OUTCOND','QUANTITATIVE']
                        for jobtag in job_intern_tags:
                            job_dict[jobtag] = []
                            xmljobtags = job.getElementsByTagName(jobtag)

                            if (len(xmljobtags)):
                                for xmljobtag in xmljobtags:
                                    job_dict[jobtag].append(dict(xmljobtag.attributes.items()))

                        ons = job.getElementsByTagName('ON')
                        job_dict['ON'] = []
                        if(len(ons)>0):
                            for on in ons:
                                on_dict = dict(on.attributes.items())

                                #iterating on children
                                on_intern_tags = ['DOCOND', 'DOFORCEJOB', 'DOACTION', 'DOMAIL']
                                for ontag in on_intern_tags:
                                    xmlontags = on.getElementsByTagName(ontag)

                                    if(len(xmlontags)):
                                        on_dict[ontag]=[]
                                        for xmlontag in xmlontags:
                                            on_dict[ontag].append(dict(xmlontag.attributes.items()))

                                job_dict['ON'].append(on_dict)
                        self.jobs.append(job_dict)
            else:
                return False
        return True

