from Model import DBModel

class Project(DBModel):
    metadata = None

    def __init__(self):
        self.mdb = DBModel()

    def get_details(self, key):
        self.metadata = self.mdb.get_project(key)
        return self.metadata

    def get_projects(self):
        return self.mdb.get_all()

    def __str__(self):
        return "Project object type"