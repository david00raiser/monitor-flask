from Model import DBModel


class Job(DBModel):
    name = None
    parents = None
    application = None
    sub_app = None
    description = None
    tasktype = None
    metadata = None

    def __init__(self, name):
        mdb = DBModel()
        data = mdb.get_job(name)
        if data:
            self.name = data["JOBNAME"]
            self.application = data["APPLICATION"]
            self.sub_app = data["SUB_APPLICATION"]
            self.description = data["DESCRIPTION"]
            self.tasktype = data["TASKTYPE"]
            self.metadata = data
        else:
            # Creates job without metadata
            self.name = name
            self.description = "Not registered"

    def get_parents(self):
        if self.metadata:
            inconds = self.metadata["INCOND"]
            arr = set()
            if len(inconds) > 0:
                for incond in inconds:
                    arr.add(Job(incond["NAME"].split("-TO-")[0]))
            self.parents = arr
            return self.parents

    def __delete__(self, instance):
        return

    def __str__(self):
        return "{} -> {}".format(self.name, self.description)
