from Model import DBModel

class FolderCTM(DBModel):
    metadata = None

    def __init__(self):
        self.mdb = DBModel()

    def get_joblist(self, key):
        return self.mdb.get_joblist_by_folder(key)

    def get_all_folders(self):
        return self.mdb.get_folders()

    def __str__(self):
        return "Project object type"