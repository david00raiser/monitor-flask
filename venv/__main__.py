from flask import Flask, render_template, url_for, request, redirect, session
from Controller import Controller
import json

app = Flask(__name__)
app.secret_key = b'\x91\xd2X.\xd5\xc1\x93\x10G4h\xea&\xbc)A'

@app.after_request
def add_header(response):
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

@app.route("/")
def index():
    return render_template('home.html', app=__name__)

@app.route("/active/")
@app.route("/active/<filter>")
def get_active(filter=None):
    if filter:
        return render_template('active.html', response = {
            'filter' : filter,
            'data' : Controller().get_status_active(filter)
        })
    else:
        if request.args.get('tag'):
            return redirect(url_for('get_active', filter=request.args.get('tag')))
        data = Controller().list_iterables()
        return render_template('active_form.html', response = {'data': data})

@app.route("/tree/")
@app.route("/tree/<job>")
def get_tree(job=None):
    if job:
        return render_template('tree.html', response = {
            'job': job,
            'tree': Controller().get_tree(job)
        })
    else:
        if request.args.get('jobname'):
            return redirect(url_for('get_tree', job=request.args.get('jobname') ))
        return render_template('tree_form.html')

@app.route("/joblist/")
@app.route("/joblist/<filter>")
def get_joblist(filter=None):
    if filter:
        return render_template('joblist.html', response = {
            'filter' : filter,
            'data' : Controller().get_job_list(filter)
        })

    else:
        if request.args.get('tag'):
            return redirect(url_for('get_joblist', filter=request.args.get('tag') ))
        data = Controller().list_iterables()
        return render_template('joblist_form.html',  response = {'data': data})

@app.route("/xml_loader")
def xml_load():
    if Controller().initial_load_xmlfiles():
        return "Parsed"
    else:
        return "Failed"

@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        data = Controller().login(request.form['username'], request.form['awspassw'])
        if data['rc'] == 0:
            session['username'] = request.form['username'].split('@')[0]
            return redirect(url_for('manager'))
        else:
            error = 'Invalid credentials'
            return render_template('login.html', error = error)
    elif 'username' in session:
        return redirect(url_for('index'))
    else:
        return render_template('login.html')

@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))

@app.route("/manager")
def manager():
    print(session)
    if 'username' in session:
        return render_template('manager.html')
    else:
        return redirect(url_for('login'))

@app.route("/logs/<job>")
def get_logs(job):
    return "Este es el modulo de logs para el job {}".format(job)

@app.errorhandler(404)
def not_found(e):
    return render_template('404.html'), 404

# with app.test_request_context():
#     print(url_for('index'))
#     print(url_for('active', job='jobX'))  active/jobX
#     print(url_for('xml_load', odate='odate')) xml_lodaer/?odate=ANY -- la funcion no espera args, entonces GET

if __name__ == '__main__':
    app.run(debug=True)
    #app.run(host='0.0.0.0')