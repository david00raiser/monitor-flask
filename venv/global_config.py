folders_path = 'resources'
daily_xml_folders = []
monthly_xml_folders = []
active_url = 'http://150.100.216.64:8080/scheduling/ajF'
#active_url = 'http://172.30.9.229:8080/scheduling/ajF'
jobs_url = 'http://172.30.9.229:8080/scheduling/planificacionesConsulta'
sysout_query_url = 'http://150.100.216.64:8080/scheduling/sysoutsConsulta'

#Mailing config : Just to notify cancelled processes
mailing_ulogin='david.huaman@bluetab.net'
mailing_plogin=''
mailing_server='smtp.gmail.com'
mailing_port=587
mailing_subject='PROCESOS PENDIENTES'
mailing_from_address='david.huaman@bluetab.net'
mailing_to_address=[
    {'email':'david.huaman.contractor@bbva.com','name': 'DAVID ARTURO HUAMAN ÑIQUEN'}
    ,{'email':'dwp-datos.group@bbva.com'}
]

#Database configs
db_name ='model'
folder_collection = 'folder'
job_collection = 'job'
logs_collection = 'logs'