from bs4 import BeautifulSoup as BS
from CustomException import CustomException
from XMLParser import XMLParser
from Job import Job
from Project import Project
from FolderCTM import FolderCTM
from anytree import Node, RenderTree
import json
from functools import reduce
from Model import DBModel

class Controller:
    def __str__(self):
        return "Controlador"

    def get_status_active(self, input_filter):
        from global_config import active_url
        full_data = []
        filters = []

        proj = Project().get_details(input_filter)
        if proj:
            filters = proj['dependencies']
        else:
            filters.append(input_filter)

        if len(filters) > 0:
            for filter in filters:
                rs = self.process(active_url, {"filtro": filter}, 'GET')
                if rs != -1:
                    if rs is not None:
                        full_data += rs
                else:
                    return CustomException("Problem in server at curl:", rs)

            data = sorted(full_data, key=lambda k: "%s %s %s" % (k['Odate'], k['Status'], k['Jobname']))
            short_data = []
            for item in data:
                json = {}
                if 1==1:# and item["Status"] != 'Ended OK':  # and item["Odate"] == '181221':
                    json["odate"] = item["Odate"]
                    json["folder"] = item["Folder"]
                    json["status"] = item["Status"].replace("¿Que espera para ejecutar?", "")
                    json["description"] = str(Job(item['Jobname']))
                    short_data.append(json)
            return short_data
            # mailer = Mailer(body)
            # mailer.sender()
        else:
            return None

    def get_job_list(self, input_filter):
        import os
        #from global_config import jobs_url

        filters = []
        proj = Project().get_details(input_filter)
        if proj:
            filters = list(filter(lambda x: x[0:2]=='CR', proj['dependencies']))
        else:
            filters.append(input_filter)

        if len(filters) > 0:
            joblist = reduce(lambda x,y: x+y,
                             list(map(lambda folder:
                                      FolderCTM().get_joblist(folder), filters )))
            linear_data = list(sorted(joblist, key=lambda k: "%s" % (k["JOBNAME"])))
            all_keys = linear_data[0].keys()
            header = list(map(lambda key: {"name": key, "title": key}, all_keys ))
            data   = list(map(lambda item: list((item['JOBNAME'], item['DESCRIPTION'], item['PARENT_FOLDER'])),linear_data))

            file_path = 'static/data.json'
            if os.path.exists(file_path):
                os.remove(file_path)

            with open(file_path, 'w') as outfile:
                json.dump({
                    'header': header,
                    'data'  : data
                }, outfile)
            return True
        else:
            return None

    def process(self, url, params, type, sysout=None):
        rc, rawdata = self.curl(url, params, type)
        if rc == 0:
            if len(rawdata) > 0:  # ok
                data = []
                for item in rawdata:
                    list = item.findAll("td")
                    json = {}
                    for elem in list:
                        json[elem.get("data-title")] = elem.text
                        if elem.get("data-title") == 'Sysout':  # and sysout:
                            sysout_link = elem.find("input")["onclick"].replace('window.open("', '').replace('");', '')
                            json[elem.get("data-title")] = sysout_link
                    data.append(json)
                return data
        else:
            return rc  # problema en curl

    def curl(self, url, params, type):
        type = type.lower()
        try:
            import requests
            if type == "get":
                response = requests.get(url, params)
            elif type == "post":
                response = requests.post(url, params)
        except:
            raise CustomException("Network problem at CURL")
        else:
            if response.status_code != 200:
                return -1, None
            else:
                html = BS(response.text, 'html.parser')
                list = html.find('table', attrs={'id': 'tblEjec'}).findChildren('tr')
                list.pop(0)  # remove table header
                return 0, list

    def get_tree(self, input_job):
        global mem_parents
        global uuaas
        global offset
        global arr_tree
        arr_tree = {}
        offset = 0
        mem_parents = {}
        uuaas = ["CDO", "DWP","DCO","IDS"]

        myjob = Job(input_job)
        arr_tree[input_job] = Node("{} - {}".format(myjob.name, myjob.description))
        self.dp(myjob)
        return self.print_tree(input_job)

    def dp(self, job, offset=0, repeat=False):
        if (job.name[1:4] in uuaas):  # valid uuaa & DP
            if len(job.get_parents()) == 0:
                return

            if (job.name in mem_parents.keys()) == False:
                mem_parents[job.name] = job.parents

                for parent in job.parents:
                    arr_tree[parent.name] = Node("{} - {}".format(parent.name, parent.description),
                                                 parent=arr_tree[job.name])
                    self.dp(parent, offset + 1)

    def print_tree(self, head_job):
        tree = ""
        for pre, fill, node in RenderTree(arr_tree[head_job]):
            tree += ("%s%s\n" % (pre, node.name))
        return tree

    def initial_load_xmlfiles(self):
        parser = XMLParser()
        parser.scan_files()
        files = parser.XMLfiles

        if len(files) > 0:
            if parser.analyze_files():
                folders = parser.folders
                jobs = parser.jobs
                for i in files: print("Parsed file:{}".format(i))

                try:

                    from global_config import folder_collection, job_collection
                    model = DBModel()
                    model.remove(folder_collection)
                    model.remove(job_collection)
                    model.save(folder_collection, folders)
                    model.save(job_collection, jobs)
                    return True
                except:
                    raise CustomException("Problem at comunicating with Cluster")

    def login(self, username, password):
        return {'rc': 0}

    def list_iterables(self):
        model = DBModel()
        folders = []
        projects = []
        for i in model.get_projects():
            projects.append(i)

        for i in model.get_folders():
            folders.append(i)

        return {
            "projects": projects,
            "folders": folders
        }